import React from 'react';
import { LandingPage } from './views/landing/LandingPage';

import 'bootstrap/dist/css/bootstrap.min.css';

const App = () => {
  return (
    <div className="App">
      <LandingPage />
    </div>
  );
};

export default App;
