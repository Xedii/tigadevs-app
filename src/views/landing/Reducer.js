import { useEffect, useMemo, useCallback, useReducer } from 'react';
import { useFetchCsv } from 'lib/hooks/FetchCsv';
import { processCsv } from 'lib/utils/ProcessCsv';
import { model } from './models/csv.model';
import _ from 'lodash';

const initialState = {
  result: null,
  error: null,
  dataSeries: [],
  campains: [],
  data: [],
  options: [],
  filterData: {},
};

const reducer = (state, action) => {
  switch (action.type) {
    case 'init':
      return initialState;
    case 'respone':
      return { ...state, data: action.result };
    case 'filterData':
      return {
        ...state,
        filterData: Object.assign(state.filterData, action.result),
        error: null,
      };
    case 'result':
      return { ...state, dataSeries: action.result, error: null };
    case 'response':
      return { ...state, result: action.result, error: null };
    case 'options':
      return { ...state, options: action.result, error: null };
    case 'error':
      return { result: null, error: 'error' };
    default:
      throw new Error('no such action type');
  }
};

const actions = dispatch => ({
  setFilter(field, data) {
    dispatch({
      type: 'filterData',
      result: {
        [field]: data,
      },
    });
  },
});

export const useReducerLanding = () => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const [response] = useFetchCsv(
    'http://adverity-challenge.s3-website-eu-west-1.amazonaws.com/DAMKBAoDBwoDBAkOBAYFCw.csv'
  );
  const options = ['campaign', 'datasource'];

  const getOptions = useCallback(
    result =>
      options.map(name => ({
        name,
        options: _.uniq(_.map(result, name)).map(el => ({
          value: el,
          label: el,
        })),
      })),
    []
  );

  const filterSelect = useCallback(
    data => {
      const hasDataSourceData =
        !_.isNil(state.filterData.datasource) &&
        state.filterData.datasource.length;
      const hasCampaignData =
        !_.isNil(state.filterData.campaign) && state.filterData.campaign.length;
      if (!hasDataSourceData && !hasCampaignData) return true;
      if (hasDataSourceData && hasCampaignData) {
        return (
          state.filterData.datasource.find(
            el => el.value === data.datasource
          ) && state.filterData.campaign.find(el => el.value === data.campaign)
        );
      } else if (hasDataSourceData) {
        return state.filterData.datasource.find(
          el => el.value === data.datasource
        );
      } else if (hasCampaignData) {
        return state.filterData.campaign.find(el => el.value === data.campaign);
      } else {
        return true;
      }
    },
    [state.filterData]
  );

  useEffect(() => {
    const result = _.chain(state.result)
      .filter(filterSelect)
      .groupBy('date')
      .map((v, i) => ({
        date: i,
        clicks: _.sumBy(v, 'clicks'),
        impressions: _.sumBy(v, 'impressions'),
      }))
      .value();

    dispatch({ type: 'result', result });
  }, [state.result, state.filterData.campaign, state.filterData.datasource]);

  useEffect(() => {
    const result = processCsv(response, model);
    dispatch({ type: 'response', result });
    dispatch({ type: 'options', result: getOptions(result) });
  }, [response]);

  const useActions = useMemo(() => actions(dispatch, state), [
    state.filterData,
  ]);

  return {
    state,
    actions: useActions,
  };
};
