export const model = newObject => ({
    date: newObject.Date,
    datasource: newObject.Datasource,
    campaign: newObject.Campaign,
    clicks: parseInt(newObject.Clicks || 0, 10),
    impressions: parseInt(newObject.Impressions || 0, 10)
})