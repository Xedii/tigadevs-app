import { useState, useEffect } from 'react';
import Papa from 'papaparse';

export const useFetchCsv = file => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const fetchUrl = async () => {
    try {
      const response = await fetch(file);
      const reader = response.body.getReader();
      const decoder = new TextDecoder('utf-8');
      const result = await reader.read();
      const csvOutput = await decoder.decode(result.value);

      Papa.parse(csvOutput, {
        complete: output => {
          setData(output.data);
        },
      });

      setLoading(false);
    } catch (err) {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchUrl();
  }, []);

  return [data, loading];
};

export default useFetchCsv;
